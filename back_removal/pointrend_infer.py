from detectron2.utils.logger import setup_logger

setup_logger()

# import some common libraries
import numpy as np
import cv2
import torch

# import some common detectron2 utilities
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg

# import PointRend project
import sys

sys.path.insert(0, "detectron2_repo/projects/PointRend")
sys.path.insert(0, "back_removal/detectron2_repo/projects/PointRend")  # for server.py
import point_rend

import os
import PIL.Image as Image


class BackRemover:
    def __init__(self, model="pretrained/pointrend_latest.pth", gpuid='0'):
        self.cfg = get_cfg()

        # not used now
        self.model = model
        self.device = torch.device("cuda:%s" % gpuid if torch.cuda.is_available() else "cpu")

        # Add PointRend-specific config
        point_rend.add_pointrend_config(self.cfg)
        # Load a config from file
        if model.split("/")[0] == "back_removal":  # if running on grpc server, a path would be changed
            self.cfg.merge_from_file("back_removal/"
                                     "detectron2_repo/projects/PointRend/configs/InstanceSegmentation/"
                                     "pointrend_rcnn_R_50_FPN_1x_coco.yaml")
        else:
            self.cfg.merge_from_file("detectron2_repo/projects/PointRend/configs/InstanceSegmentation/"
                                     "pointrend_rcnn_R_50_FPN_1x_coco.yaml")
        self.cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.5  # set threshold for this model
        self.cfg.MODEL.DEVICE = "cuda:%s" % gpuid if torch.cuda.is_available() else "cpu"

        self.cfg.MODEL.WEIGHTS = model
        self.predictor = DefaultPredictor(self.cfg)

    def remove_back(self, img_file):
        if isinstance(img_file, str):
            clothes_im = np.array(Image.open(img_file).convert("RGBA"))
        elif isinstance(img_file, np.ndarray):
            clothes_im = np.array(Image.fromarray(img_file).convert("RGBA"))
        else:
            raise TypeError("The image input should be a filepath (str) or image array (np.array).")

        background_mask = (clothes_im[:, :, 3] < 127)
        clothes_im[background_mask, :3] = 255

        with torch.no_grad():
            # Detectron2 are officially trained with BGR image
            outputs = self.predictor(clothes_im[:, :, 2::-1])  # RGB to BGR
            pred_result = outputs["instances"].to("cpu")

        if pred_result.has("pred_masks"):
            classes = np.asarray(pred_result.pred_classes) if pred_result.has("pred_classes") else None
            masks = np.asarray(pred_result.pred_masks.permute(1, 2, 0))
        else:
            raise RuntimeError("There is no mask detected.")

        if classes is not None:
            front_mask = np.zeros((masks.shape[0], masks.shape[1]), dtype=bool)  # contents mask init
            for idx, cls in enumerate(classes):
                if cls == 0:
                    front_mask = np.logical_or(front_mask, (masks[:, :, idx] != 0))
        else:
            raise RuntimeError("There is no region detected as clothes")

        back_mask = np.logical_and(np.logical_not(background_mask), np.logical_not(front_mask))

        front_image = np.zeros((clothes_im.shape[0], clothes_im.shape[1], 4), dtype=np.uint8)
        front_image[front_mask, 3] = 255
        front_image[front_mask, :3] = clothes_im[front_mask, :3]

        back_image = np.zeros((clothes_im.shape[0], clothes_im.shape[1], 4), dtype=np.uint8)
        back_image[back_mask, 3] = 255
        back_image[back_mask, :3] = clothes_im[back_mask, :3]

        return {'front': front_image, 'back': back_image}


if __name__ == '__main__':
    img_file = os.path.join('..', 'test_data', 'sample_image.png')
    im = cv2.imread(img_file, cv2.IMREAD_UNCHANGED)
    im[:, :, :3] = im[:, :, 2::-1]

    background_remover = BackRemover()
    results = background_remover.remove_back(im)

    front_contents = Image.fromarray(results['front']).convert("RGBA")
    front_contents.save("../test_data/front_segmented.png")
    back_contents = Image.fromarray(results['back']).convert("RGBA")
    back_contents.save("../test_data/back_segmented.png")
