# Copyright (c) Facebook, Inc. and its affiliates. All Rights Reserved

# Keep this module for backward compatibility.

__all__ = ["Registry"]
