import cv2
import numpy as np


def get_warped_image(from_pts, to_pts, diang, image):
    h, w = image.shape[0], image.shape[1]
    masked_image = np.zeros((h, w, 4), dtype=np.uint8)  # pretend RGBA image
    masked_image[:, :, 3] = 0

    to_pts = to_pts.astype(np.int32)
    x_min, x_max = np.amin(to_pts[:, 0]), np.amax(to_pts[:, 0])
    y_min, y_max = np.amin(to_pts[:, 1]), np.amax(to_pts[:, 1])

    for t in diang[:]:
        mask = np.zeros(image.shape, dtype=np.uint8)
        cv2.fillPoly(mask, np.array([[to_pts[t[0]], to_pts[t[1]], to_pts[t[2]]]], dtype=np.int32), (255, 255, 255),
                     cv2.LINE_AA)
        mask_true = np.logical_and(np.logical_and(mask[:, :, 0] == 255, mask[:, :, 1] == 255), mask[:, :, 2] == 255)
        mask[mask_true, 3] = 255

        from_triangle = np.float32([from_pts[t[0]], from_pts[t[1]], from_pts[t[2]]])
        to_triangle = np.float32([to_pts[t[0]], to_pts[t[1]], to_pts[t[2]]])
        affine_t = cv2.getAffineTransform(from_triangle, to_triangle)

        masked_image = cv2.bitwise_or(masked_image, cv2.bitwise_and(cv2.warpAffine(image, affine_t, (w, h)), mask))

    return masked_image[y_min:y_max, x_min:x_max, :], {'x_min': x_min, 'x_max': x_max, 'y_min': y_min, 'y_max': y_max}


def denoise_triangulation(warped_image, strength=5):
    # get transparent mask with alpha channel
    mask_false = warped_image[:, :, 3] < 127
    denoised_cropped = np.zeros(warped_image.shape, dtype=np.uint8)
    denoised_cropped[:, :, 3] = 255

    # denoise an image to remove pixel noises in concatenating triangles one another
    denoised_image = cv2.fastNlMeansDenoisingColored(warped_image, None, strength, 10, 21, 7)

    # get RGB from denoised image
    denoised_cropped[..., :3] = denoised_image[..., :3]
    # get alpha mask from the original result
    denoised_cropped[mask_false, 3] = 0

    return denoised_cropped
