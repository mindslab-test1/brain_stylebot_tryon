import pickle as pkl
import sys

import PIL.Image as Image
import numpy as np
import os
from scipy.spatial import Delaunay

sys.path.insert(0, 'try_on')
from triangulate.functions import get_warped_image, denoise_triangulation
from util.frame_kp import get_frame_keypoint, NUM_CLASSES
from util.kp_utils import add_margin_point, to_keypoint

AVATAR_W = 507
AVATAR_H = 1347


class ClothesTryOn:
    def __init__(self, avatar_file="avatar_sample/sample_avatar_openarm.png"):

        if os.getcwd().split("/")[-1] != 'try_on':
            avatar_file = "try_on/avatar_sample/sample_avatar_openarm.png"

        try:
            self.avatar_im = Image.open(avatar_file).resize((AVATAR_W, AVATAR_H))
            self.avatar_np = np.array(self.avatar_im)

        except FileNotFoundError:
            raise FileNotFoundError("File {:s} does not exist!!!".format(avatar_file))

        # Due to weird .png format, we convert into RGB handling with numpy array.
        self.avatar_rgb = self.avatar_np[:, :, :3]
        self.avatar_rgb[self.avatar_np[:, :, 3] == 0, :] = 0

        # if frame_keypoint is not None:
        #     frame_kp = to_frame_keypoint(frame_keypoint)
        # else:
        #     frame_kp = get_frame_keypoint(args.class_id)
        self.frame_kp_list = get_frame_keypoint()

    def get_warped_clothes(self, clothes_img_dict, clothes_kp, class_id=-1, frame_kp=None):

        # assertion check
        if not class_id in range(0, NUM_CLASSES):
            raise AssertionError("class_id should be from 0 to {:1d}.".format(NUM_CLASSES - 1))

        if frame_kp is not None:
            frame_kp = to_keypoint(frame_kp)
            frame_kp_margin = add_margin_point(frame_kp)
        else:
            frame_kp_margin = self.frame_kp_list[class_id]

        use_separate_back = False
        if not isinstance(clothes_img_dict, dict):
            raise TypeError(
                "The input of clothes_img_dict should be dict() whose keys are 'front' and 'back' (option).")
        else:
            try:
                # Assume that the H, W of front_clothes are both 512 (decided by segmentation API)
                # It should be bigger than or (at least) equal to AVATAR_H.
                h, w = clothes_img_dict['front'].shape[:2]  # In fact, h = w (decided by segmentation API)
                front_clothes = Image.fromarray(clothes_img_dict['front']).resize((AVATAR_H, AVATAR_H))  # RGBA Image
                front_clothes = np.array(front_clothes)

                clothes_kp = clothes_kp / float(h) * AVATAR_H  # adjust both scales of h and w
                clothes_kp_margin = add_margin_point(clothes_kp)
                delaunay_triangles = Delaunay(clothes_kp_margin)

                if 'back' in clothes_img_dict.keys():
                    use_separate_back = True

            except KeyError:
                raise KeyError("clothes_img_dict should have keys named 'front' and 'back' (option).")

        front_transparent = (front_clothes[:, :, 3] == 0)
        front_clothes[front_transparent, :3] = 0

        warped_front, boundary = get_warped_image(clothes_kp_margin, frame_kp_margin, delaunay_triangles.simplices,
                                                  front_clothes)

        # Because we use .png with alpha channel, there is a noise in concatenating triangles on another.
        # To remove this, we apply denoising method.
        denoised_front = denoise_triangulation(warped_front)  # RGBA Image numpy array

        if use_separate_back:
            # Same assumption with front clothes
            back_clothes = Image.fromarray(clothes_img_dict['back']).resize((AVATAR_H, AVATAR_H))  # RGBA Image
            back_clothes = np.array(back_clothes)
            back_transparent = (back_clothes[:, :, 3] == 0)
            back_clothes[back_transparent, :3] = 0
            warped_back, _ = get_warped_image(clothes_kp_margin, frame_kp_margin, delaunay_triangles.simplices,
                                              back_clothes)
            denoised_back = denoise_triangulation(warped_back)  # RGBA Image numpy array
        else:
            denoised_back = None

        return {'front': denoised_front, 'back': denoised_back, 'boundary': boundary}

    def wear_clothes(self, warped_clothes_dict, with_avatar_image=False):

        if with_avatar_image:
            avatar_pil = Image.fromarray(self.avatar_np)
        else:
            avatar_vacant = np.zeros((AVATAR_H, AVATAR_W, 4), dtype=np.uint8)
            avatar_pil = Image.fromarray(avatar_vacant)

        front_pil = Image.fromarray(warped_clothes_dict['front'])
        boundary = warped_clothes_dict['boundary']
        x_min, y_min = boundary['x_min'], boundary['y_min']

        avatar_pil.paste(front_pil, (x_min, y_min), front_pil)
        avatar_with_front = np.array(avatar_pil)

        # For now, server.py doesn't combine clothes with avatar image
        if warped_clothes_dict['back'] is not None and with_avatar_image:  # pretend that it is with_avatar_image
            back_pil = Image.fromarray(warped_clothes_dict['back'])
            back_contents_mask = (back_pil[:, :, 3] != 0)
            avatar_transparency_mask = (avatar_with_front[:, :, 3] == 0)
            back_fill_mask = np.logical_and(back_contents_mask, avatar_transparency_mask)

            avatar_with_front[back_fill_mask, :3] = back_contents_mask[back_fill_mask, :3]
            avatar_with_front[back_fill_mask, 3] = 255
        else:
            pass

        return avatar_with_front


def main():
    avatar_file = "../test_data/sample_avatar_openarm.png"
    clothes_file = "../test_data/sample_image.png"
    clothes_kp_file = "../test_data/sample_image.pkl"

    clothes_tryon = ClothesTryOn(avatar_file)

    clothes_front = Image.open(clothes_file).convert("RGBA")
    clothes_img_dict = {'front': np.array(clothes_front)}

    with open(clothes_kp_file, 'rb') as f:
        clothes_kp = pkl.load(f)

    warped_clothes = clothes_tryon.get_warped_clothes(clothes_img_dict, clothes_kp, class_id=1)
    avatar_worn = clothes_tryon.wear_clothes(warped_clothes, with_avatar_image=True)

    avatar_worn_pil = Image.fromarray(avatar_worn).convert("RGBA")
    avatar_worn_pil.save("../test_data/sample_avatar_worn.png")


if __name__ == '__main__':
    main()
