import cv2
import numpy as np


def plot_kp(img, coords):
    img2 = np.copy(img)
    h, w = img2.shape[:2]
    for coord in coords:
        cv2.circle(img2, (coord[0], coord[1]), 2, [255, 0, 0], 2)

    return img2
