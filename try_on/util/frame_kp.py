import numpy as np

from .kp_utils import to_keypoint, add_margin_point

NUM_CLASSES = 13


def get_frame_keypoint():
    # Keypoints are written in x, y order
    frame_kp_list = [np.array([]) for _ in range(NUM_CLASSES)]  # num_classes=13
    # Long Sleeve Top (긴팔티)
    # frame_kp_list[1] = np.array([[253, 224], [211, 216], [228, 257], [253, 266], [278, 257], [295, 216],
    #        [143, 241], [109, 332], [101, 457], [ 84, 548], [ 79, 640], [126, 640], [143, 548],
    #        [160, 482], [160, 432], [160, 399], [163, 424], [163, 482], [143, 615], [253, 631], [380, 615],
    #        [354, 482],[354, 424], [358, 399], [358, 432], [358, 482], [375, 548], [392, 640], [439, 640],
    #        [434, 548], [408, 457], [408, 332], [375, 241]]) # x, y order

    # given by the client
    frame_kp_list[1] = to_keypoint([
        255, 245, 183, 233, 211, 262, 255, 270, 300, 263, 328, 234, 114, 261, 97, 370, 85, 466,
        66, 536, 38, 695, 96, 702, 118, 578, 148, 473, 155, 419, 156, 326, 152, 366, 156, 490,
        135, 625, 254, 640, 376, 626, 359, 491, 359, 367, 355, 327, 356, 420, 364, 477, 392,
        577, 412, 702, 467, 694, 446, 537, 427, 467, 415, 371, 397, 262
    ])  # x, y order

    # Trousers (긴바지)
    frame_kp_list[7] = np.array([[152, 548], [253, 565], [371, 548], [135, 748], [152, 914],
                                 [152, 1197], [236, 1197], [245, 931], [253, 731], [270, 931], [261, 1197], [354, 1197],
                                 [354, 914], [371, 748]])  # x, y order

    for idx in range(NUM_CLASSES):
        if frame_kp_list[idx].shape[0] != 0:
            frame_kp_list[idx] = add_margin_point(frame_kp_list[idx])

    return frame_kp_list
