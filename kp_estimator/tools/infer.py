from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import collections
import pickle as pkl
import sys

import PIL.Image as Image
import PIL.ImageOps as ImageOps
import cv2
import numpy as np
import os
import torch
import torch.nn.parallel
import torch.optim
import torch.utils.data
import torch.utils.data.distributed
import torchvision.transforms as transforms

sys.path.insert(0, 'kp_estimator/tools')
this_dir = os.path.dirname(__file__)
sys.path.insert(0, os.path.join(this_dir, '..', 'lib'))
sys.path.insert(0, os.path.join(this_dir, '..', 'lib/poseeval/py-motmetrics'))

from config import cfg
from config import update_config
from core.inference import get_max_preds
from models.pose_hrnet import get_pose_net
from utils.values import KP_MAP_AGGREGATE, KP_REGION_DICT

# from utils.vis import save_debug_images


RESIZE_H = 512
RESIZE_W = 384

NUM_CATEGORIES = len(KP_MAP_AGGREGATE)
NUM_REGIONS = len(KP_REGION_DICT)
REGION_LIST = list(KP_REGION_DICT.values())

DefaultArgs = collections.namedtuple("args", ['save_img', 'save_keypoint', 'use_width_pad'])

def parse_args():
    parser = argparse.ArgumentParser(description='Infer keypoints network')
    # general
    parser.add_argument('--cfg',
                        help='experiment configure file name',
                        type=str,
                        default='configs/w48_512x384_adam_lr1e-3-agg81kps.yaml')
    parser.add_argument('opts',
                        help="Modify config options using the command-line",
                        default=None,
                        nargs=argparse.REMAINDER)
    parser.add_argument('--model_dir',
                        help='model directory',
                        type=str,
                        default='')
    parser.add_argument('--log_dir',
                        help='log directory',
                        type=str,
                        default='')
    parser.add_argument('--data_dir',
                        help='data directory',
                        type=str,
                        default='')
    parser.add_argument('--prev_model_dir',
                        help='prev Model directory',
                        type=str,
                        default='')

    # only for inference
    parser.add_argument('--checkpoint_path',
                        help='path for checkpoint (default: pretrained/final_state.pth)',
                        type=str,
                        default='pretrained/final_state.pth')

    parser.add_argument('--class_id',
                        help='clothes category (required)',
                        type=int,
                        default=-1)

    parser.add_argument('--gpuid',
                        help='gpuid',
                        type=int,
                        default=0)

    parser.add_argument('--save_img', action='store_true',
                        help='whether to save image whose keypoints are marked')
    parser.add_argument('--save_keypoint', action='store_true',
                        help='whether to save keypoints as a .pkl file')
    parser.add_argument('--use_width_pad', action='store_true',
                        help='whether to add padding on left and right (default=True)')

    parser.set_defaults(use_width_pad=True)

    args = parser.parse_args()
    return args


def _extract_keypoints(image, batch_joints, class_id, margin, width_pad=0):
    ndarr = np.array(image)
    scale_x = (ndarr.shape[1] + 2 * margin['w_margin']) / 96
    scale_y = (ndarr.shape[0] + 2 * margin['h_margin']) / 128

    cat_idx = [x - 1 for x in KP_MAP_AGGREGATE[class_id]]
    joints = batch_joints[0][cat_idx, :]  # 81 x 2

    if width_pad:
        margin['w_margin'] += width_pad

    joints[:, 0] = (joints[:, 0] * scale_x) - margin['w_margin']
    joints[:, 1] = (joints[:, 1] * scale_y) - margin['h_margin']
    joints.astype(np.int32)
    return joints


# originally from utils.vis
def _save_image_with_keypoints(image, joints, file_name, width_pad=0):
    ndarr = np.array(image)
    joints_vis = [1 for _ in range(joints.shape[0])]  # not used now

    if width_pad:
        ndarr = ndarr[:, width_pad:-width_pad, :]

    for joint, joint_vis in zip(joints, joints_vis):
        cv2.circle(ndarr, (joint[0], joint[1]), 2, [255, 0, 0], 2)

    cv2.imwrite(file_name, ndarr[:, :, ::-1])


def _save_keypoints(joints, file_name):
    with open(file_name, 'wb') as f:
        pkl.dump(joints, f)


def _add_width_pad(pil_image):
    h, w = pil_image.height, pil_image.width
    width_pad = w // 10
    return ImageOps.expand(pil_image, (width_pad, 0)), width_pad


def _fit_image_ratio(pil_image):
    h, w = pil_image.height, pil_image.width
    if h / w > RESIZE_H / RESIZE_W:  # if h is too large
        new_w = int(h * RESIZE_W / RESIZE_H)
        new_image = ImageOps.expand(pil_image, ((new_w - w) // 2, 0))
        return new_image, {'w_margin': (new_w - w) // 2, 'h_margin': 0, 'scale': h / RESIZE_H}
    else:
        new_h = int(w * RESIZE_H / RESIZE_W)
        new_image = ImageOps.expand(pil_image, (0, (new_h - h) // 2))
        return new_image, {'w_margin': 0, 'h_margin': (new_h - h) // 2, 'scale': w / RESIZE_W}


class KPEstimator:
    def __init__(self, model_path, gpuid='0', args=None):

        if args is not None:
            self.args = args
            # Add args in cfg, but not used here. It will be fixed. See lib.config.defaults
            update_config(cfg, self.args)
        else:
            self.args = DefaultArgs(False, False, True)
            cfg.defrost()
            cfg.merge_from_file('kp_estimator/configs/w48_512x384_adam_lr1e-3-agg81kps.yaml')
            cfg.GPUS = "(%s, )" % gpuid
            cfg.freeze()

        self.device = torch.device("cuda:%s" % gpuid if torch.cuda.is_available() else "cpu")
        self.model = get_pose_net(cfg, is_train=False)
        self.model.to(self.device)

        self.model_path = model_path
        self.model.load_state_dict(torch.load(self.model_path))
        self.model.eval()

        # image to tensor
        self.transform_tensor = transforms.Compose([
            transforms.Resize((RESIZE_H, RESIZE_W), interpolation=Image.BICUBIC),
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
        ])

    def get_keypoint(self, img_file, class_id):

        if class_id == -1:
            raise AssertionError("class_id should be from 0 to 12.")

        if isinstance(img_file, str):
            clothes_im = np.array(Image.open(img_file).convert("RGBA"))
        elif isinstance(img_file, np.ndarray):
            clothes_im = np.array(Image.fromarray(img_file).convert("RGBA"))
        else:
            raise TypeError("The image input should be a filepath (str) or image array (np.array).")

        background_mask = (clothes_im[:, :, 3] < 127)
        clothes_im[background_mask, :3] = 255

        img_pil = Image.fromarray(clothes_im).convert("RGB")

        # for better result, it needs padding for each width
        if self.args.use_width_pad:
            img_pil, width_pad = _add_width_pad(img_pil)
        else:
            width_pad = 0

        # adjust an image to be a square while keeping ratio
        img_pil_square, scale_result = _fit_image_ratio(img_pil)

        # get tensor from PIL.Image
        img_tensor = self.transform_tensor(img_pil_square).unsqueeze(0)

        # compute output
        with torch.no_grad():
            outputs = self.model(img_tensor.to(self.device))

            if isinstance(outputs, list):
                output = outputs[-1]
            else:
                output = outputs

            heatmap = output.detach().cpu().numpy()
            preds, maxvals = get_max_preds(heatmap)
            maxvals = np.squeeze(maxvals)

        cat_score = np.zeros(NUM_CATEGORIES)
        region_score = np.zeros(NUM_REGIONS)

        for cat in range(NUM_CATEGORIES):
            cat_idx = [x - 1 for x in KP_MAP_AGGREGATE[cat]]
            cat_score[cat] = np.median(maxvals[cat_idx])
        for region in range(NUM_REGIONS):
            region_idx = [x - 1 for x in REGION_LIST[region]]
            region_score[region] = np.median(maxvals[region_idx])

        joints = _extract_keypoints(img_pil, preds, class_id, scale_result, width_pad=width_pad)

        if self.args.save_img:

            if not isinstance(img_file, str):
                raise AssertionError("The input should have been filepath, not numpy array.")

            _save_image_with_keypoints(img_pil, joints, os.path.splitext(img_file)[0] + "_converted.jpg",
                                       width_pad=width_pad)

        if self.args.save_keypoint:

            if not isinstance(img_file, str):
                raise AssertionError("The input should have been filepath, not numpy array.")

            _save_keypoints(joints, os.path.splitext(img_file)[0] + ".pkl")

        return joints


def main():
    args = parse_args()
    estimator = KPEstimator(args.checkpoint_path, gpuid=args.gpuid, args=args)

    img_path = "../test_data/sample_image.png"

    keypoint = estimator.get_keypoint(img_path, args.class_id)
    print("class_id:", args.class_id)
    print("keypoint.shape:", keypoint.shape)


if __name__ == '__main__':
    main()
