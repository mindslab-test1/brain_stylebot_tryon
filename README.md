# Stylebot Try-On API

## Usage

### Setup
```bash
python -m grpc.tools.protoc --proto_path=. --python_out=. --grpc_python_out=. stylbot-try-on.proto
```

### Run Server
MODEL_BACK_PATH, MODEL_KP_PATH는 필수
```bash
python server.py --model_back_removal MODEL_BACK_PATH --model_kp_estimation MODEL_KP_PATH \
--gpuid GPUID --port PORT --log_level LOG_LEVEL
```

### Run Client
 IMG_PATH, CLASS_ID 필수
```bash
python client.py -r GRPC_IP_PORT -i IMG_PATH -c CLASS_ID
```


## Authors
@hksong35@mindslab.ai  
@myaowng2@mindslab.ai  
@acasia@mindslab.ai  
@changho@mindslab.ai  

## Review
### gRPC
`2020.09.16` 
@hwern@mindslab.ai  