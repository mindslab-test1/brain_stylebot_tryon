FROM nvcr.io/nvidia/cuda:10.1-cudnn7-runtime-ubuntu18.04

WORKDIR /app

COPY requirements.txt ./

RUN set -xe \
 && apt-get -y update \
 && apt-get install -y python-pip python3-dev
RUN pip install --upgrade pip
RUN apt-get -y update \
 && apt-get install -y git vim libsm6 libxext6 libxrender-dev
EXPOSE 50061

ENV PYTHONIOENCODING UTF-8

RUN apt-get install -y python3-venv
RUN python3 -m venv venv_stylebot_tryon
#RUN source venv5/bin/activate
RUN venv_stylebot_tryon/bin/pip3 install --upgrade pip
RUN venv_stylebot_tryon/bin/pip3 install -r requirements.txt
RUN venv_stylebot_tryon/bin/pip3 install detectron2 -f https://dl.fbaipublicfiles.com/detectron2/wheels/cu101/torch1.4/index.html

COPY . ./stylebot-tryon
WORKDIR stylebot-tryon

ENTRYPOINT ../venv_stylebot_tryon/bin/python3 server.py --model_back_removal back_removal/pretrained/pointrend_latest.pth --model_kp_estimation kp_estimator/pretrained/final_state.pth --gpuid 1