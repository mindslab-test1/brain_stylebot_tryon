import os
import json
import numpy as np
import cv2
from tqdm import tqdm
from PIL import Image
import numpy as np
from skimage import measure
from shapely.geometry import Polygon, MultiPolygon


def create_sub_masks(mask_image):
    width, height = mask_image.size

    # Initialize a dictionary of sub-masks indexed by RGB colors
    sub_masks = {}
    for x in range(width):
        for y in range(height):
            # Get the RGB values of the pixel
            pixel = mask_image.getpixel((x,y))[:3]

            # If the pixel is not black...
            if pixel != (0, 0, 0):
                # Check to see if we've created a sub-mask...
                pixel_str = str(pixel)
                sub_mask = sub_masks.get(pixel_str)
                if sub_mask is None:
                   # Create a sub-mask (one bit per pixel) and add to the dictionary
                    # Note: we add 1 pixel of padding in each direction
                    # because the contours module doesn't handle cases
                    # where pixels bleed to the edge of the image
                    sub_masks[pixel_str] = Image.new('1', (width+2, height+2))

                # Set the pixel value to 1 (default is 0), accounting for padding
                sub_masks[pixel_str].putpixel((x+1, y+1), 1)

    return sub_masks


def create_sub_mask_annotation(sub_mask, image_id, category_id, annotation_id, is_crowd, separate_poly=False):
    # Find contours (boundary lines) around each sub-mask
    # Note: there could be multiple contours if the object
    # is partially occluded. (E.g. an elephant behind a tree)
    contours = measure.find_contours(np.array(sub_mask), 0.5, positive_orientation='low')

    segmentations = []
    polygons = []
    annotation = []
    for contour in contours:
        # Flip from (row, col) representation to (x, y)
        # and subtract the padding pixel
        for i in range(len(contour)):
            row, col = contour[i]
            contour[i] = (col - 1, row - 1)

        # Make a polygon and simplify it
        try:
            poly = Polygon(contour)
            poly = poly.simplify(1.0, preserve_topology=False)
            polygons.append(poly)
            segmentation = np.array(poly.exterior.coords).ravel().tolist()
            segmentations.append(segmentation)
        except AttributeError:
            pass

    # Combine the polygons to calculate the bounding box and area
    if not separate_poly:
        multi_poly = MultiPolygon(polygons)
        x, y, max_x, max_y = multi_poly.bounds
        width = max_x - x
        height = max_y - y
        bbox = (x, y, width, height)
        area = multi_poly.area

        annotation.append({
        'segmentation': segmentations,
        'iscrowd': is_crowd,
        'image_id': image_id,
        'category_id': category_id,
        'id': annotation_id,
        'bbox': bbox,
        'area': area
        })
    else:
        cnt = 0
        for idx in range(len(polygons)):
            try:
                poly_list = np.array(polygons[idx].exterior.coords)
                x, y = np.amin(poly_list, axis=0)
                max_x, max_y = np.amax(poly_list, axis=0)
                width = max_x - x
                height = max_y - y
                bbox = (x, y, width, height)
                area = polygons[idx].area

                annotation.append({
                'segmentation': [poly_list.ravel().tolist()],
                'iscrowd': is_crowd,
                'image_id': image_id,
                'category_id': category_id,
                'id': annotation_id + cnt,  # TODO: sync with the index in outer scope
                'bbox': bbox,
                'area': area
                })
                cnt += 1
            except ValueError:
                pass
            except AttributeError:
                pass

    return annotation


def make_base_dict():
    dataset = {
        "info": {},
        "licenses": [],
        "images": [],
        "annotations": []
    }

    dataset['categories'] = [
        {
            'id': 1,
            'name': "foreground",
            'supercategory': "clothes_part",
            'keypoints': [],
            'skeleton': []
        },
        {
            'id': 2,
            'name': "back",
            'supercategory': "clothes_part",
            'keypoints': [],
            'skeleton': []
        },
        {
            'id': 3,
            'name': "hanger",
            'supercategory': "clothes_part",
            'keypoints': [],
            'skeleton': []
        }]

    return dataset


def get_image_filepath(root_dir, clothes_types=None):
    if clothes_types is None:
        raise AssertionError("Clothes types should be declared.")
    files = []
    for clothes_type in clothes_types:
        type_dir = os.path.join(root_dir, clothes_type)
        files.extend(get_listdir(type_dir))

    return files

def get_listdir(root_dir):
    return sorted([os.path.join(root_dir, x) for x in os.listdir(root_dir)
                   if os.path.splitext(x)[-1] in ['.jpg', '.png', '.jpeg', '.JPG', '.PNG', '.JPEG']])

if __name__ == '__main__':

    mask_dir = '/DATA1/hksong/stylebot-20200831/mask'
    image_dir = '/DATA1/hksong/stylebot-20200831/original'
    json_name = '/DATA1/hksong/stylebot-20200831/annotation.json'

    clothes_types = ['dress', 'outer', 'pants', 'skirt', 'top']

    print("get_image_filepath")
    image_list = get_image_filepath(image_dir, clothes_types=clothes_types)
    print("get_mask_filepath")
    mask_list = get_image_filepath(mask_dir, clothes_types=clothes_types)

    need_resize = False  # FIXME: make it as argparse
    mask_images = []
    for idx in tqdm(range(len(mask_list))):
        if need_resize:
            mask_image = Image.open(mask_list[idx]).convert("RGB").resize((1024, 1024), Image.ANTIALIAS)
            mask_images.append(mask_image)
            mask_image.save(mask_list[idx])
        else:
            mask_image = Image.open(mask_list[idx]).convert("RGB")
            mask_images.append(mask_image)

    if need_resize:
        for idx in tqdm(range(len(image_list))):
            clothes_image = Image.open(image_list[idx]).convert("RGB").resize((1024, 1024), Image.ANTIALIAS)
            clothes_image.save(image_list[idx])

    mask_num = len(mask_list)
    print("The number of mask images: ", mask_num)

    # make COCO base json
    dataset = make_base_dict()

    # Define which colors match which categories in the images
    foreground, back, hanger = [1, 2, 3]
    category_ids = {
        '(255, 255, 255)': foreground,
        '(255, 0, 0)': back,
        '(0, 0, 255)': hanger,
    }

    is_crowd = 0

    # These ids will be automatically increased as we go
    annotation_id = 1

    for idx in tqdm(range(mask_num)):
        mask_image = mask_images[idx]
        file_name_path = '/'.join(mask_list[idx].split('/')[-2:])

        image_id = idx + 1

        width, height = mask_image.size
        dataset['images'].append({
            'coco_url': '',
            'date_captured': '',
            'file_name': file_name_path,
            'flickr_url': '',
            'id': image_id,
            'license': 0,
            'width': width,
            'height': height
        })
        sub_masks = create_sub_masks(mask_image)
        for color, sub_mask in sub_masks.items():
            try:
                category_id = category_ids[color]
            except KeyError:
                continue

            if color == '(255, 0, 0)':
                annotations = create_sub_mask_annotation(sub_mask, image_id, category_id, annotation_id, is_crowd,
                                                         separate_poly=True)
            else:
                annotations = create_sub_mask_annotation(sub_mask, image_id, category_id, annotation_id, is_crowd,
                                                         separate_poly=False)
            for annotation in annotations:
                dataset['annotations'].append(annotation)
                annotation_id += 1

    with open(json_name, 'w') as f:
        json.dump(dataset, f)
    f.close()
