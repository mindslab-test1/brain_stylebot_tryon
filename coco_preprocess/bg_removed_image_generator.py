import numpy as np
import os
from PIL import Image
from tqdm import tqdm


def get_image_filepath(root_dir, clothes_types=None):
    if clothes_types is None:
        raise AssertionError("Clothes types should be declared.")
    files = []
    for clothes_type in clothes_types:
        type_dir = os.path.join(root_dir, clothes_type)
        files.extend(get_listdir(type_dir))

    return files


def get_listdir(root_dir):
    return sorted([os.path.join(root_dir, x) for x in os.listdir(root_dir)
                   if os.path.splitext(x)[-1] in ['.jpg', '.png', '.jpeg', '.JPG', '.PNG', '.JPEG']])


if __name__ == '__main__':

    mask_dir = '/DATA1/hksong/stylebot-20200831/mask'
    image_dir = '/DATA1/hksong/stylebot-20200831/original'
    new_image_dir = '/DATA1/hksong/stylebot-20200831/bg_removed'
    clothes_types = ['dress', 'outer', 'pants', 'skirt', 'top']

    # make directory first if not exists
    os.makedirs(new_image_dir, exist_ok=True)
    for clothes_type in clothes_types:
        os.makedirs(os.path.join(new_image_dir, clothes_type), exist_ok=True)

    print("get_image_filepath")
    image_list = get_image_filepath(image_dir, clothes_types=clothes_types)
    print("get_mask_filepath")
    mask_list = get_image_filepath(mask_dir, clothes_types=clothes_types)

    need_resize = False  # FIXME: make it as argparse
    mask_images = []
    for idx in tqdm(range(len(mask_list))):
        if need_resize:
            mask_image = Image.open(mask_list[idx]).convert("RGB").resize((1024, 1024), Image.ANTIALIAS)
            mask_images.append(mask_image)
            mask_image.save(mask_list[idx])
        else:
            mask_image = Image.open(mask_list[idx]).convert("RGB")
            mask_images.append(mask_image)

    if need_resize:
        for idx in tqdm(range(len(image_list))):
            clothes_image = Image.open(image_list[idx]).convert("RGB").resize((1024, 1024), Image.ANTIALIAS)
            clothes_image.save(image_list[idx])

    mask_num = len(mask_list)
    print("The number of mask images: ", mask_num)

    for idx in tqdm(range(mask_num)):
        mask_image = np.array(mask_images[idx])
        file_name_path = '/'.join(mask_list[idx].split('/')[-2:])
        clothes_image = np.array(Image.open(os.path.join(image_dir, file_name_path)).convert("RGB"))
        clothes_no_bg = np.copy(clothes_image)
        mask_contents = mask_image[:, :, 0] == 255
        # fill background as white
        clothes_no_bg[np.logical_not(mask_contents), :] = 255
        clothes_no_bg_pil = Image.fromarray(clothes_no_bg)
        clothes_no_bg_pil.save(os.path.join(new_image_dir, file_name_path))
